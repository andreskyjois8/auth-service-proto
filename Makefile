.PHONY: gen-all gen publish

gen-all:
	protoc --go_out=. --go_opt=paths=source_relative \
		--go-grpc_out=. --go-grpc_opt=paths=source_relative \
		**/*.proto

gen:
	@read -p "Enter Path (ex. auth/auth.proto): " name; \
	protoc --go_out=. --go_opt=paths=source_relative \
		--go-grpc_out=. --go-grpc_opt=paths=source_relative \
		$$name

publish:
	@read -p "Enter new version: " NEW_VERSION && \
	read -p "Enter tag message: " TAG_MESSAGE && \
	OLD_VERSION=$$(git describe --tags --abbrev=0) && \
	if [ "$$OLD_VERSION" != "" ]; then \
		echo "Deleting existing tag: $$OLD_VERSION" && \
		git tag -d $$OLD_VERSION && \
		git push --delete origin refs/tags/$$OLD_VERSION; \
	fi && \
	git tag -a $$NEW_VERSION -m "$$TAG_MESSAGE" && \
	git push origin refs/tags/$$NEW_VERSION